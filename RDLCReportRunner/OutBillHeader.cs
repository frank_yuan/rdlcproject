﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RDLCReportRunner
{
    class OutBillHeader
    {
        public string CustomerCode { get; set; }
        public string BillNo { get; set; }
        public string CustomizedField1 { get; set; }
    }
}
