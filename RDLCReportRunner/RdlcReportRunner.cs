﻿namespace RDLCReportRunner
{
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Threading.Tasks;
    using Microsoft.Reporting.WebForms;

    public class RdlcReportRunner
    {
        private const string FileExtension = "xls";
        private string rdlcReportName;

        private IEnumerable<ReportDataSource> reportData;

        private IEnumerable<ReportParameter> reportParams; 

        public Dictionary<string, string> ReportParameters { get; set; }

        public string OutputPath { get; set; }

        public string ReportDataFile { get; set; }

        public string RdlcReportName { get; set; }

        public string OutputFileName
        {
            get { return rdlcReportName + "." + FileExtension; }
            set { rdlcReportName = value; }
        }

        public byte[] GenerateReport()
        {
            //GetReportDataFromService();
            GetReportParametersFromDictionary();

            return RunLocalReport();
        }

        private void GetReportParametersFromDictionary()
        {
            reportParams = ReportParameters.Select(parameter => new ReportParameter(parameter.Key, parameter.Value));
        }
        public void WriteReportToFile(byte[] renderedReportBytes)
        {
            var fullFilePath = Path.Combine(OutputPath, OutputFileName);
            if (File.Exists(fullFilePath))
            {
                File.Delete(fullFilePath);
            }

            if (renderedReportBytes != null)
            {
                using (var fs = new FileStream(fullFilePath, FileMode.Create))
                {
                    fs.Write(renderedReportBytes, 0, renderedReportBytes.Length);
                }
            }
        }
        private void GetReportDataFromService()
        {
            
            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("ProductCode", typeof(string));
            dt.Columns.Add("ProductName", typeof(string));
            dt.Columns.Add("UnitCode", typeof(string));
            dt.Columns.Add("UnitName", typeof(string));
            dt.Columns.Add("BillQuantity", typeof(decimal));
            dt.Columns.Add("AllotQuantity", typeof(decimal));
            dt.Columns.Add("RealQuantity", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));
            for (int i = 0; i < 5; i++)
            {
                dt.Rows.Add
                        (
                            "item.ProductCode",
                            "item.ProductName",
                            "item.UnitCode",
                            "item.UnitName",
                            i,
                            i,
                            i,
                            "item.Description"
                        );
            }
            
            System.Data.DataTable dth = new System.Data.DataTable();
          
            dth.Columns.Add("BillNo", typeof(string));
            dth.Columns.Add("RefNo", typeof(string));

            dth.Rows.Add("IN2014098232","123ere22");

            List<ReportDataSource> dtList = new List<ReportDataSource>();
            dtList.Add(new ReportDataSource("GRDetail", dt));
            dtList.Add(new ReportDataSource("GRHeader", dth));
            reportData = dtList;
        }
        public void addDataTable(string tableName, DataTable dt)
        {
            List<ReportDataSource> dtList = new List<ReportDataSource>();
            if (reportData != null)
            {
               dtList = reportData.ToList();
            }
            dtList.Add(new ReportDataSource(tableName, dt));
            reportData = dtList;
        }
        private void GetReportDataFromXml()
        {
            var ds = new DataSet();
            
            if (!string.IsNullOrEmpty(ReportDataFile))
            {
                ds.ReadXml(ReportDataFile);
            }

            if (ds.Tables.Count == 0)
            {
                return;
            }
            reportData = (from DataTable dt in ds.Tables select new ReportDataSource(dt.TableName, dt)).ToList();
        }

       
        private static string GetReportDeviceInfo()
        {
            //// The DeviceInfo settings should be changed based on the reportType
            //// http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            return "<DeviceInfo><OutputFormat>Excel</OutputFormat></DeviceInfo>";
        }

        private byte[] RunLocalReport()
        {
            using (var localReport = new LocalReport { ReportPath = RdlcReportName })
            {
                byte[] renderedBytes;
                try
                {
                    using (var textReader = new StreamReader(RdlcReportName))
                    {
                        localReport.LoadReportDefinition(textReader);
                    }

                    if (reportData != null)
                    {
                        foreach (var ds in reportData)
                        {
                            localReport.DataSources.Add(ds);
                        }
                    }

                    if (reportParams != null && reportParams.Any())
                    {
                        localReport.SetParameters(reportParams);
                    }

                    //configure page setting

                    ReportPageSettings pageSetting1 = localReport.GetDefaultPageSettings();
                    

                    var deviceInfo = GetReportDeviceInfo();

                    localReport.Refresh();
                   

                    string mimeType;
                    string encoding;
                    string fileNameExtension;
                    Warning[] warnings;
                    string[] streams;
                    renderedBytes = localReport.Render(
                        "Excel",
                        deviceInfo,
                        out mimeType,
                        out encoding,
                        out fileNameExtension,
                        out streams,
                        out warnings);
                }
                catch (Exception e)
                {
                    Console.WriteLine("{0}", e.Message);
                    return null;
                }

                return renderedBytes;
            }
        }
    }
}