﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RDLCReportRunner
{
    public class OutBillDetail
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string CustomizedField1 { get; set; }//货品批号
        public string CustomizedField2 { get; set; }//出分配序列号
        public string ProductNetWheight { get; set; }//货品规格
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public string AllotQuantity { get; set; }
        public string CellName { get; set; }
        public string NetWheight { get; set; }
    }
}
