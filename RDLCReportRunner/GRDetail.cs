﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RDLCReportRunner
{
    public class GRDetail
    {
        public string ProductCode{ get;set;}
        public string ProductName { get; set; }
        public string ProductNetWeight { get; set; }
        public string UnitCode { get; set; }
        public string UnitName { get; set; }
        public decimal BillQuantity { get; set; }
        public decimal AllotQuantity { get; set; }
        public decimal RealQuantity { get; set; }
        public string Description { get; set; }
        public string CustomizedField1 { get; set; }
        public string CustomizedField2 { get; set; }
        public string CustomizedField3 { get; set; }
        public string CustomizedField4 { get; set; }
        public string CustomizedField5 { get; set; }
    }
}
