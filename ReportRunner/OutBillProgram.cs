﻿namespace ReportRunner
{
    using System;
    using System.Collections.Generic;
    using RDLCReportRunner;

    public class OutBillProgram
    {
        public static void Main(string[] args)
        {
            var paramDictionary = new Dictionary<string, string> {{"param1", "Hello"}, {"param2", "back!"}};

            var rdlcReportRunner = new RdlcReportRunner
            {
                OutputPath = @"c:\temp", // the output directory
                OutputFileName = "TestReportOutput", // the output file name - no extension is required
                ReportDataFile = null, // an xml file with the report datasources
                RdlcReportName = @"OutBillPrinterForm.rdlc",  // the actual RDLC report to render with the data
                ReportParameters = paramDictionary // key value pairs with parameter name and parameter value
            };

            //test update dt
            System.Data.DataTable dth = new System.Data.DataTable();
            dth.Columns.Add("CustomerCode", typeof(string));
            dth.Columns.Add("BillNo", typeof(string));
            dth.Columns.Add("CustomizedField1", typeof(string));

            dth.Rows.Add("3432423", "3423423","1111");
            rdlcReportRunner.addDataTable("OutBillHeader", dth);

            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("CustomizedField2", typeof(string));
            dt.Columns.Add("ProductCode", typeof(string));
            dt.Columns.Add("ProductName", typeof(string));
            dt.Columns.Add("CustomizedField1", typeof(string));
            dt.Columns.Add("ProductNetWheight", typeof(string));
            dt.Columns.Add("UnitName", typeof(string));
            dt.Columns.Add("AllotQuantity", typeof(decimal));
            dt.Columns.Add("CellName", typeof(string));
            dt.Columns.Add("NetWheight", typeof(string));
            for (int i = 0; i < 5; i++)
            {
                dt.Rows.Add
                        (
                            "0726061",
                            "BY-02536122",
                            "desc",
                            "p9p1000865",
                            280,
                            "桶",
                            5,
                            "C02-22",
                            "12000"
                            
                        );
            }
           rdlcReportRunner.addDataTable("OutBillDetail", dt);

            //end of test
            var reportData = rdlcReportRunner.GenerateReport(); // run the report and return the bytes[] - can be used on a website to return a stream to the user
            rdlcReportRunner.WriteReportToFile(reportData); // write the report to an actual file on the local directory

            Console.WriteLine("Report run completed.");
            Console.ReadKey();
        }
    }
}
