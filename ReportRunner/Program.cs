﻿namespace ReportRunner
{
    using System;
    using System.Collections.Generic;
    using RDLCReportRunner;

    public class Program
    {
        public static void Main2(string[] args)
        {
            var paramDictionary = new Dictionary<string, string> {{"param1", "Hello"}, {"param2", "back!"}};

            var rdlcReportRunner = new RdlcReportRunner
            {
                OutputPath = @"C:\Temp", // the output directory
                OutputFileName = "TestReportOutput", // the output file name - no extension is required
                ReportDataFile = null, // an xml file with the report datasources
                RdlcReportName = @"GoodsReceiptPrinterForm.rdlc",  // the actual RDLC report to render with the data
                ReportParameters = paramDictionary // key value pairs with parameter name and parameter value
            };

            //test update dt
            System.Data.DataTable dth = new System.Data.DataTable();
            dth.Columns.Add("BillNo", typeof(string));
            dth.Columns.Add("CustomerCode", typeof(string));
            dth.Columns.Add("CustomizedField1", typeof(string));

            dth.Rows.Add("3432423", "3423423","c33333");
            rdlcReportRunner.addDataTable("GRHeader", dth);

            System.Data.DataTable dt = new System.Data.DataTable();
            dt.Columns.Add("ProductCode", typeof(string));
            dt.Columns.Add("ProductName", typeof(string));
            dt.Columns.Add("UnitCode", typeof(string));
            dt.Columns.Add("UnitName", typeof(string));
            dt.Columns.Add("BillQuantity", typeof(decimal));
            dt.Columns.Add("AllotQuantity", typeof(decimal));
            dt.Columns.Add("RealQuantity", typeof(decimal));
            dt.Columns.Add("Description", typeof(string));
            for (int i = 0; i < 5; i++)
            {
                dt.Rows.Add
                        (
                            "item.ProductCode",
                            "item.ProductName",
                            "item.UnitCode",
                            "item.UnitName",
                            i,
                            i,
                            i,
                            "item.Description"
                        );
            }
            rdlcReportRunner.addDataTable("GRDetail", dt);

            //configure bill out
            System.Data.DataTable dtod = new System.Data.DataTable();
            dtod.Columns.Add("BillNo", typeof(string));
            dtod.Columns.Add("BillQuantity", typeof(decimal));
            dtod.Columns.Add("GrossWeight", typeof(decimal));
            for (int i = 0; i < 5; i++)
            {
                dtod.Rows.Add
                        (
                            "BillNo"+i,
                            i,
                            i                            
                        );
            }
            rdlcReportRunner.addDataTable("OutBillDetail", dtod);
            //end of test
            var reportData = rdlcReportRunner.GenerateReport(); // run the report and return the bytes[] - can be used on a website to return a stream to the user
            rdlcReportRunner.WriteReportToFile(reportData); // write the report to an actual file on the local directory

            Console.WriteLine("Report run completed.");
            Console.ReadKey();
        }
    }
}
